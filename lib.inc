section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx
.loop:
    cmp byte [rdi], 0
    jz .loop_end
    inc rdi
    inc rcx
    jmp .loop
.loop_end:
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
section .text
print_newline:
    mov rdi, 10

; Принимает код символа и выводит его в stdout
section .data
char: db 0
section .text
print_char:
    mov [char], rdi
    mov rax, 1
    mov rsi, char
    mov rdx, 1
    mov rdi, 1
    syscall
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и хр8
; Храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    xor r12, r12
    mov rax, rdi
    xor rdx, rdx
.loop:
    mov r8, 10
    div r8
    push rdx
    inc r12
    xor rdx, rdx
    cmp rax, 0
    jz .print_loop
    jmp .loop
.print_loop:
    pop rdi
    add rdi, 48
    call print_char
    dec r12
    test r12, r12
    jz .loop_end
    js .loop_end
    jmp .print_loop
.loop_end:
    pop r12
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx
.loop:
    movzx rax, byte [rdi]
    movzx rbx, byte [rsi]
    test rax, rax
    jz .check_for_second_word_end
    test rbx, rbx
    jz .not_equals
    cmp rax, rbx
    jne .not_equals
    inc rdi
    inc rsi
    jmp .loop
.check_for_second_word_end:
    test rbx, rbx
    jz .equals
.not_equals:
    xor rax, rax
    jmp .end
.equals:
    mov rax, 1
.end:
    pop rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
section .data
buffer: db 0
section .text
read_char:
    xor eax, eax
    xor edi, edi
    mov rsi, buffer
    mov edx, 1
    syscall
    test eax, eax
    je end_of_stream
    movzx rax, byte [buffer]
    ret
end_of_stream:
    xor rax, rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    dec r13
    push rdi
.loop:
    call read_char
    mov rdx, rax
    test r13, r13
    jz .err_end
    cmp rdx, 0x20
    je .is_word_read
    cmp rdx, 0x9
    je .is_word_read
    cmp rdx, 0xA
    je .is_word_read
    mov [r12], rdx
    test rdx, rdx
    jz .loop_end
    inc r12
    dec r13
    inc r14
    jmp .loop
.is_word_read:
    test r14, r14
    jz .loop
.loop_end:
    pop rax
    mov rdi, rax
    push rax
    call string_length
    mov rdx, rax
    pop rax
    jmp .end
.err_end:
    pop rdi
    xor rax, rax
.end:
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r8, r8
    cmp byte[rdi], '0'
    jb .not_digit
    cmp byte[rdi], '9'
    ja .not_digit
.loop:
    cmp byte[rdi], '0'
    jb .end
    cmp byte[rdi], '9'
    ja .end
    cmp byte[rdi], 0
    jz .end
    imul r8, 10
    movzx r9, byte[rdi]
    sub r9, 48
    add r8, r9
    dec rcx
    inc rdi
    jmp .loop
.not_digit:
    xor rdx, rdx
    ret
.end:
    sub rdx, rcx
    mov rax, r8
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor r8, r8
    cmp byte[rdi], 0
    jz .not_digit
    cmp byte[rdi], '+'
    jz .parse
    cmp byte[rdi], '-'
    jz .neg
    cmp byte[rdi], '0'
    jb .not_digit
    cmp byte[rdi], '9'
    ja .not_digit
    jmp .parse
.neg:
    mov r8, 1
    inc rdi
.parse:
    push r8
    call parse_uint
    pop r8
    test rdx, rdx
    jz .not_digit
    test r8, r8
    jz .end
    neg rax
    inc rdx
    jmp .end
.not_digit:
    xor rax, rax
    xor rdx, rdx
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rcx
    cmp rax, rdx
    jg .err_end
    test rax, rax
    je .end
.loop:
    mov al, byte [rdi]
    test al, al
    jz .end
    mov byte [rsi], al
    inc rdi
    inc rsi
    jmp .loop
.err_end:
    pop rax
    xor rax, rax
    ret
.end:
    mov byte [rsi], 0
    pop rax
    ret